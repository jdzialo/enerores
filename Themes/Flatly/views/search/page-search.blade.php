@extends('layouts.master')
@section('content')
<div class="pg_default">
    <div class="pg_header">
        <div class="header_content">
            <div class="title">Wyniki wyszukiwania</div>
            <div class="box_breadcrumb">
                <a href="{{ URL::to('/') }}">Strona główna</a>
                <span>Wyniki wyszukiwania</span>
            </div>
        </div>
        <div class="line_top"></div>
        <div class="line_bottom"></div>
    </div>
    <div class="container">
        <div class="row row_default">
            @if($items->count())
            @foreach($items as $key => $item)
            <div class="col-xs-12">
                <div class="box_search_item text">
                    <h2><a href="/{{$item->slug}}">{{$item->title}}</a></h2>
                    <div class="date">{{ $item->created_at->format('d-m-Y') }}</div>
                    <div class="short">{!! $item->short !!}</div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-xs-12">
                <p class="bg-info">Nie znaleziono szukanej frazy.</p>
            </div>
            @endif               
        </div>
    </div>
</div>
@endsection
