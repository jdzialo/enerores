@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
<div class="pg_home">
    @include('partials.main-slider')
    <div class="container">
        @include('partials.home-categories')
        @if(count($defaultslider))
        @include('partials.banner-slider')
        @endif
    </div>
</div>
@endsection


@push('style')
{!! Theme::style('vendor/swiper/dist/css/swiper.min.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/swiper/dist/js/swiper.min.js') !!}
@endpush
