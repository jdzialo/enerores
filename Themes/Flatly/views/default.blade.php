@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@endsection
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@endsection

@section('content')
<div class="pg_default">
    <div class="pg_header">
        <div class="line_top"></div>
        <div class="header_content">
            <div class="title">{{ $page->title }}</div>
            <div class="box_breadcrumb">
                <a href="{{ URL::to('/') }}">Strona główna</a>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span>{{ $page->title }}</span>
            </div>
        </div>
        <div class="line_bottom"></div>
    </div>
    <div class="container pg_container">
        <div class="row">
            <div class="col-xs-12 text">
                @if($page->body)
                <h1>{!! $page->body !!}</h1>
                @endif

                @if($page->short)
                <div class="short">
                    {!! $page->short !!}
                </div>
                @endif
                {!! $page->content !!}
            </div>
        </div>
        <div class="row row_gallery">
            @if($images != null)
            @foreach($images as $file)
            <div class="col-md-3">
                <img src="{{ $file->getThumbnail('mediumThumb') }}" class="img-responsive img-thumbnail" >
            </div>
            @endforeach
            @endif
        </div>
        @if(count($defaultslider))
        @include('partials.banner-slider')
        @endif
    </div>
</div>
@endsection

@push('style')
{!! Theme::style('vendor/swiper/dist/css/swiper.min.css') !!}
@endpush
@push('scripts')
{!! Theme::script('vendor/swiper/dist/js/swiper.min.js') !!}
@endpush
