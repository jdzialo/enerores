@extends('layouts.master')
@section('content')
<div class="pg_default">
    <div class="pg_header">
        <div class="header_content">
            <div class="title">KONTAKT</div>
            <div class="box_breadcrumb">
                <a href="{{ URL::to('/') }}">Strona główna</a>
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span>KONTAKT</span>
            </div>
        </div>
        <div class="line_top"></div>
        <div class="line_bottom"></div>
    </div>
    <div class="container">
        <div class="row row_default">
            <div class="col-xs-12 col-sm-6 contactdiv">
                <div class="text">
                    <h2>ENERGORES Sp. z o.o.</h2>
                    Adres : 35-310 Rzeszów, <br/>
                    Ul: Rejtana 10a <br/>
                    Tel: 0-17 852-13-02, <br/>

                    Dział handlowy: <br/>
                    Fax 852-13-02, <br/>
                    Tel.: 852-30-68 

                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col_form">
                {!! Form::open(['route' => 'contactStore', 'method' => 'post', 'class' => 'contactform']) !!}
                @include('contact.partials.create-fields', ['lang' => locale(), 'errors' => $errors])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="box_gmap">
        <div id="gmap_contact"></div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxrjy4S3oZppA94ztine1IIr2oAdx4NPc&region=PL"></script>
{!! Theme::script('vendor/gmap3/dist/gmap3.min.js') !!}
<script>
    $(document).ready(function () {
        $('#gmap_contact').gmap3({
            map: {
                address: "Rzeszów",
                options: {
                    zoom: 12,
                    scrollwheel: false,
                    mapTypeId: "style1",
                    mapTypeControlOptions: {
                        mapTypeIds: ["style1"]
                    }
                }
            },
            marker: {address: "Rejtana 10a, Rzeszów", options: {icon: "/themes/flatly/img/icons/gmap_marker_blue.png"}},
            styledmaptype: {
                id: "style1",
                options: {name: "Map"},
                styles: gmapStyle
            }
        });
    });
</script>

<script>
    var gmapStyle = [{"featureType": "landscape", "stylers": [{"hue": "#F1FF00"}, {"saturation": -27.4}, {"lightness": 9.4}, {"gamma": 1}]}, {"featureType": "road.highway", "stylers": [{"hue": "#0099FF"}, {"saturation": -20}, {"lightness": 36.4}, {"gamma": 1}]}, {"featureType": "road.arterial", "stylers": [{"hue": "#00FF4F"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]}, {"featureType": "road.local", "stylers": [{"hue": "#FFB300"}, {"saturation": -38}, {"lightness": 11.2}, {"gamma": 1}]}, {"featureType": "water", "stylers": [{"hue": "#00B6FF"}, {"saturation": 4.2}, {"lightness": -63.4}, {"gamma": 1}]}, {"featureType": "poi", "stylers": [{"hue": "#9FFF00"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]}];
</script>
@endpush
