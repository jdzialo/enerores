<div class="box-body">
    <div class="text">
        <div class="contact_title">
            <h2>{!! trans('page::contact.form.contactformname') !!}</h2>
        </div>
        <div class="claus">{{ trans('page::contact.contact.agree') }}</div>
    </div>
    
    
    <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('name', trans('page::contact.form.name'), array('class' => 'requiredfield')) !!}
            </div>
            <div class="col-md-8">
                {!! Form::text("name", old("name"), ['class' => 'form-control', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Nazwa"', 'placeholder' => trans('page::contact.form.name')]) !!}
            </div>
            {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('email', trans('page::contact.form.email'), array('class' => 'requiredfield')) !!}
            </div>
            <div class="col-md-8">
                {!! Form::email("email", old("email"), ['class' => 'form-control', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="E-mail"',  'placeholder' => trans('page::contact.form.email')]) !!}
            </div>
            {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    
    <div class='form-group{{ $errors->has("subject") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('subject', trans('page::contact.form.subject'), array('class' => 'requiredfield')) !!}  
            </div>
            <div class="col-md-8">
                {!! Form::text("subject", old("subject"), ['class' => 'form-control', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Temat"',  'placeholder' => trans('page::contact.form.subject')]) !!}
            </div>
            {!! $errors->first("subject", '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class='form-group{{ $errors->has("contactmessage") ? ' has-error' : '' }}'>
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('contactmessage', trans('page::contact.form.message'), array('class' => 'requiredfield')) !!}
            </div>
            <div class="col-md-8">
                {!! Form::textarea("contactmessage", old("contactmessage"), ['class' => 'form-control', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Wiadomość"',  'placeholder' => trans('page::contact.form.message'), 'rows' => 3]) !!}
            </div>
            {!! $errors->first("contactmessage", '<span class="help-block">:message</span>') !!}
        </div>
    </div>
      
    <div class="form-group">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <button class="btn_diamond_blue" type="submit"><span>{{ trans('page::contact.button.send') }}</span></button>
            </div>
        </div>
    </div>
</div>