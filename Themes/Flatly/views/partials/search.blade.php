<div class="modal fade box_search" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Wyszukaj na stronie:
                <button type="button" class="btn_diamond_red pull-right" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'pages-lists', 'method' => 'get', 'class' => 'searchform']) !!}
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="titlesearch" class="form-control" placeholder="Wyszukaj" value="{{ old('titlesearch') }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn_diamond_blue"><span>Szukaj</span></button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
