<footer class="container-fluid">
    <div class="container">
        <div class="row row_menu">
            <div class="col-md-2 col_logo">
                <img src="/themes/flatly/img/logo_bottom.png" alt="logo energores">
            </div>
            <div class="hidden-xs col-sm-12 col-md-10 col_menu">
                @menu('Menu')
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="line_gray"></div>
            </div>
        </div>
        <div class="row row_contact col_address">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="ico_marker"></div>35-310 Rzeszów, ul. Rejtana 10a
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3 col_phone">
                <div class="ico_phone"></div>0-17 852-13-02
            </div>
            <div class="col-sm-6 col-md-2 col-lg-3 col_mobile">
                <div class="ico_mobile"></div>602 388 115
            </div>
            <div class="col-sm-6 col-md-3 col_email">
                <div class="ico_email"></div>
                <a href="mailto:energores@rze.pl">energores@rze.pl</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="line_gray"></div>
            </div>
        </div>
        <div class="row row_copyright">
            <div class="col-xs-12 col-sm-6">Copyright 2016</div>
            <div class="col-xs-12 col-sm-6 col_right">Projekt i realizacja strony internetowej: <a href="http://www.moonbite.pl">moonbite.pl</a></div>
        </div>
    </div>
</footer>
