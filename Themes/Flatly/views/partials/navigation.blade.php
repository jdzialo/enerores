<header class="container-fluid">
    <div class="container">
        <div class="row row_top">
            <div class="col-xs-12 col-sm-6">
                Profesjonalne systemy klimatyzacji i wentylacji
            </div>
            <div class="col-xs-12 col-sm-6 col_r">
                Masz pytania? Zadzwoń: <b>+48 602 388 115</b>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-default main_menu">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="btn_search visible-xs-inline-block visible-sm-inline-block" data-toggle="modal" data-target="#myModal">
                <span class="ico_search"></span> 
                <span class="lbl_search">SZUKAJ</span>
            </button>
            <button type="button" class="navbar-toggle btn_hamburger" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}" >
                <img src="/themes/flatly/img/logo.png" alt="logo">
            </a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse pull-right">
            @menu('Menu')
            <ul class="nav navbar-nav nav_search hidden-xs hidden-sm">
                <li>
                    <button type="button" class="btn_search" data-toggle="modal" data-target="#myModal">
                        <span class="ico_search"></span> 
                        <span class="lbl_search">SZUKAJ</span>
                    </button> 
                </li>
            </ul>
        </div>
    </div>
</nav>
@include('partials.search')
