@php
$banners[] = '/themes/flatly/img/tmp/banner_01.png';
$banners[] = '/themes/flatly/img/tmp/banner_02.png';
$banners[] = '/themes/flatly/img/tmp/banner_03.png';
$banners[] = '/themes/flatly/img/tmp/banner_04.png';
$banners[] = '/themes/flatly/img/tmp/banner_05.png';
$banners[] = '/themes/flatly/img/tmp/banner_06.png';
$banners[] = '/themes/flatly/img/tmp/banner_07.png';
$banners[] = '/themes/flatly/img/tmp/banner_08.png';
@endphp


<div class="row_banner_slider">
    <div class="swiper-container">
        <div class="title_wrapper">
            <div class="title">NASI PARTNERZY</div>
        </div>
        <div class="swiper-wrapper">
            @foreach($defaultslider as $file)
            <div class="swiper-slide">
                <img src="{{$file->path}}" alt="banner">
            </div>
            @endforeach
        </div>
        <div class="swiper-button-prev">
            <div class="ico_arrow_prev"></div>
        </div>
        <div class="swiper-button-next">
            <div class="ico_arrow_next"></div>
        </div>
        <div class="mask_white"></div>
    </div>
</div>


@push('scripts')
<script>
    $(document).ready(function () {
        var swiper = new Swiper('.swiper-container', {
            paginationClickable: true,
            effect: 'slide',
            loop: 'true',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            slidesPerView: 5,
            //autoplay: '2000',
            breakpoints: {
                992: {
                    slidesPerView: 4,
                    spaceBetween: 40
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                460: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });
    });
</script>
@endpush