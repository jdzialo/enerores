@php
$homeCategories[] = array(
'title' => 'KLIMATYZACJA', 
'content' => 'Zapewnij sobie komfort w upalne dni. Stwórz klimat, który pozwoli na odpoczynek...',
'img' => '/themes/flatly/img/kategorie/klimatyzacja.jpg',
'slug' => 'klimatyzacja'
);
$homeCategories[] = array(
'title' => 'Wentylacja', 
'content' => 'Wentylacja pomieszczeń jest bardzo ważna zarówno dla bezpieczeństwa jak i odpowiedniego...',
'img' => '/themes/flatly/img/kategorie/wentylacja.jpg',
'slug' => 'wentylacja'
);
$homeCategories[] = array(
'title' => 'Rekuperacja', 
'content' => 'Dobierz produkty dostosowane do Twojego mieszkania. Skorzystaj z naszych...',
'img' => '/themes/flatly/img/kategorie/rekuperacja.jpg',
'slug' => 'rekuperacja'
);
$homeCategories[] = array(
'title' => 'Pompy ciepła', 
'content' => 'Profesjonalne pompy ciepła, które sprawdzą się w każych warunkach...',
'img' => '/themes/flatly/img/kategorie/pompy_ciepla.jpg',
'slug' => 'pompy-ciepla'
);
$homeCategories[] = array(
'title' => 'Elektryczne systemy grzejne', 
'content' => 'Zastosowanie najwyższej jakości materiałów, pozwala nam na...',
'img' => '/themes/flatly/img/kategorie/elektryczne_systemy_grzejne.jpg',
'slug' => 'elektryczne-systemy-grzejne'
);
@endphp
<div class="home_categories">
    @foreach($homeCategories as $categories)
    <div class="box_home_categories">
        <div class="img_wrapper">
            <img src="{{$categories['img']}}" class="img-responsive">
        </div>
        <div class="content_wrapper">
            <div class="title">
                {{$categories['title']}}
            </div>
            <div class="content">
                {{$categories['content']}}
                <a href="/{{$categories['slug']}}" class="read_more">czytaj więcej <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="cl"></div>
</div>