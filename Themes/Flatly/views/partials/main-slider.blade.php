@php
$slides[] = array(
'title' => 'Przyjazny klimat w twoim domu',
'content' => 'Będąc w gronie rodzinnym dbasz o ich bezpieczeństwo. Zadbaj również o ich komfort i zdrowie. Sprawdź naszą ofertę urządzeń do klimatyzacji, rekuperacji, pomp ciepła...',
'img' => '/themes/flatly/img/tmp/slide_1.jpg'
);
$slides[] = array(
'title' => 'Lorem ipsum dolor sit amet',
'content' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium ...',
'img' => '/themes/flatly/img/tmp/slide_2.jpg'
);
$slides[] = array(
'title' => 'Consectetur adipiscing elit',
'content' => 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit...',
'img' => '/themes/flatly/img/tmp/slide_3.jpg'
);
@endphp

<div class="container_main_slider">
    <div class="mswiper-container">
        <div class="swiper-wrapper">
            @foreach($slider as $slide)
            <div class="swiper-slide" style="background-image: url('{{ $slide->file->getThumbnail('semiThumb') }}')">
                <div class="triangle_white_mask"></div>
                <div class="text_box animated">
                    <div class="title">{{$slide->title}}</div>
                    <div class="content">{{$slide->content}}</div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="line_gray"></div>
        <div class="navigation_box">
            <div class="mask_white"></div>
            <div class="swiper-button-prev">
                <div class="ico_arrow_prev"></div>
            </div>
            <div class="swiper-button-next">
                <div class="ico_arrow_next"></div>
            </div>
        </div>
    </div>
</div>



@push('scripts')
<script>
    var mswiper;
    $(document).ready(function () {
        mswiper = new Swiper('.mswiper-container', {
            slidesPerView: 1,
            effect: 'fade',
            loop: 'true',
            //autoplay: '3000',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev'
        });
        mswiper.on('slideChangeStart', function () {
            $('.swiper-slide .text_box').removeClass('slideInDown');
            $('.swiper-slide .text_box').hide();
        });
        mswiper.on('onSlideChangeEnd', function () {
            $('.swiper-slide .text_box').show();
            $('.swiper-slide .text_box').addClass('slideInDown');
        });
        $('.swiper-slide .text_box').show();
        $('.swiper-slide .text_box').addClass('slideInDown');
    });
</script>
@endpush
