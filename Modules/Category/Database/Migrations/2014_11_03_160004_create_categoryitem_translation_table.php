<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryitemTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category__categoryitem_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('categoryitem_id')->unsigned();
            $table->string('locale')->index();

            $table->tinyInteger('status')->default(0);
            $table->string('title');
            $table->string('url')->nullable();
            $table->string('uri')->nullable();

            // $table->unique(['categoryitem_id', 'locale']);
            $table->foreign('categoryitem_id')->references('id')->on('category__categoryitems')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category__categoryitem_translations');
    }
}
