<?php

namespace Modules\Category\Events;

class CategoryItemWasCreated
{
    public $categoryItem;

    public function __construct($categoryItem)
    {
        $this->categoryItem = $categoryItem;
    }
}
