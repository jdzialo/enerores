<div class='form-group{{ $errors->has('name') ? ' has-error' : '' }}'>
    {!! Form::label('name', trans('category::category.form.name')) !!}
    {!! Form::text('name', old('name', $category->name), ['class' => 'form-control', 'placeholder' => trans('category::category.form.name')]) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>
