<?php

return [
    'form' => [
        'firstname' => 'Imię',
        'lastname' => 'Nazwisko',
         'name' => 'Nazwa',
        '_name' => 'Imie i Nazwisko',
         'contactformname' => 'Formularz kontaktowy',
        'email' => 'E-mail',
        'phone' => 'Telefon',
        'message' => 'Wiadomość',
        'instalation_location' => 'Opis miejsca instalacji',
        'subject' => 'Temat',
        'file_cv' => 'Załącz CV'
    ],
    'button' =>
    [
        'send' => 'Wyślij wiadomość',
        'send2' => 'Wyślij'
    ],
     'contact' =>
    [
        'data'=>'Dane kontaktowe',
        'send contactform' => 'Wysłałeś formularze kontaktowy',
         'receive message' => 'Otrzymałeś wiadomość od' ,
         'agree' => 'Wyślij list. Wymagane jest wypełnienie wszystkich pól oznaczonych gwiazdką *.' , 
         'receive offer message' => 'Otrzymałeś wiadomość od' ,
          'send offer' => 'Wysłałeś oferte',
    
    ],
    'mail' => [
        'subject' => "Formularz kontaktowy",
         'offersubject' => "Oferta"
    ],
    'name is required' => 'Nazwa jest wymagana',
    
    'email is required' => 'Adres e-mail jest wymagany',
    'phone is required' => 'Nr telefonu jest wymagany',
    'email is not valid' => 'Podano nieporpawny adres e-mail',
     'name is not valid' => 'Nazwa jest nieprawidłowa',
    'message is required' => 'Wiadomość jest wymagana',
    'captcha is required' => 'Captcha jest wymagana',
    'subject is required' => 'Temat jest wymagany',
    'subject is not valid' => 'Temat jest nieprawidłowy',
];
