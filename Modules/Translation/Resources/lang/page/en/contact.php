<?php

return [
    'form' => [
        'firstname' => 'First Name',
        'lastname' => 'Last Name',
        'name' => 'Name',
        '_name' => 'Full name',
        'contactformname' => 'Contact form',
        'email' => 'E-mail',
        'phone' => 'Phone',
        'message' => 'Message',
        'subject' => 'Subject',
        'instalation_location' => 'description of instalation'
    ],
    'button' =>
        [
        'send' => 'Send message'
    ],
    'contact' =>
        [
        'data' => 'Contact data',
        'send contactform' => 'You send contact form',
        'receive message' => 'You received a message from',
        'receive offer message' => 'Otrzymałeś wiadomość od',
        'agree' => 'Wyślij list. Wymagane jest wypełnienie wszystkich pól oznaczonych gwiazdką *.'
    ],
    'mail' => [
        'subject' => "Formularz kontaktowy"
    ],
    'name is required' => 'Name is required',
  
    'email is required' => 'Email is required',
    'phone is required' => 'Phone is required',
    'email is not valid' => 'Email is not valid',
    'name is not valid' => 'Name is not valid',
    'message is required' => 'Message is required',
    'captcha is required' => 'Captcha is required',
    'subject is required' => 'Subject is required',
    'subject is not valid' => 'Subject is not valid',
];
