<?php

return [
    'form' => [
        'page' => 'Strona',
        'module' => 'Moduł',
        'target' => 'Cel',
        'same tab' => 'Ta sam karta',
        'new tab' => 'Nowa karta',
        'icon' => 'Ikona',
        'class' => 'Klasa CSS',
        'parent menu item' => 'Rodzic',
    ],
    'link-type' => [
        'link type' => 'Typ odnośnika',
        'page' => 'Strona',
        'internal' => 'Wewnętrzny',
        'external' => 'Zewnętrzny',
    ],
    'list resource' => 'List menu items',
    'create resource' => 'Create menu items',
    'edit resource' => 'Edit menu items',
    'destroy resource' => 'Delete menu items',
];
