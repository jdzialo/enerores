<?php

return [
    'login' => 'Login',
    'email' => 'Email',
    'remember me' => 'Pamiętaj mnie',
    'forgot password' => 'Zapomianłem hasła',
    'register' => 'Rejstracja nowego użytkownika',
    'password' => 'Hasło',
    'password confirmation' => 'Powtórz hasło',
    'register me' => 'Rejstracja',
    'I already have a membership' => 'I already have a membership',
    'reset password' => 'Resetuj hasło',
    'I remembered my password' => 'I remembered my password',
    'to reset password complete this form' => 'To reset your password, complete this form: ',
    'welcome title' => 'Welcome',
    'please confirm email' => 'Please confirm your email address by clicking the link below.',
    'additional confirm email message' => 'We may need to send you critical information about our service and it is important that we have an accurate email address.',
    'confirm email button' => 'Confirm email address',
    'end greeting' => 'The team',
    'sign in welcome message' => 'Sign in to start your session',
    'reset password email was sent' => 'Reset password email was sent',
];
