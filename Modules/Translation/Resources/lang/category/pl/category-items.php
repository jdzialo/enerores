<?php

return [
    'form' => [
        'page' => 'Strona',
        'module' => 'Moduł',
        'target' => 'Cel',
        'same tab' => 'Ta sam karta',
        'new tab' => 'Nowa karta',
        'icon' => 'Ikona',
        'class' => 'Klasa CSS',
        'parent category item' => 'Rodzic',
    ],
    'link-type' => [
        'link type' => 'Typ odnośnika',
        'page' => 'Strona',
        'internal' => 'Wewnętrzny',
        'external' => 'Zewnętrzny',
    ],
    'list resource' => 'List category items',
    'create resource' => 'Create category items',
    'edit resource' => 'Edit category items',
    'destroy resource' => 'Delete category items',
];
