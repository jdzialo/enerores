<?php

return [
    'title' => [
        'slider' => 'Slider',
        'edit slider' => 'Edytuj slider',
        'create slider' => 'Utwórz slider'
    ],
    'popup' => [
        'slide_content' => 'Opis slajdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
    'button' => [
        'create slider' => "Utwórz slider"
    ],
    'form' => [
        'title' => 'Tytuł'
    ]
];

