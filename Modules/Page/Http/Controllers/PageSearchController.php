<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Blog\Repositories\PostRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Dashboard\Services\StatisticsService;
use Modules\Page\Http\Requests\CreateContactFormRequest;
use Modules\Page\Http\Requests\CreateRecruitmentFormRequest;
use Illuminate\Support\Facades\Input;

class PageSearchController extends BasePublicController {

    /**

     * Get the index name for the model.

     *

     * @return string

     */
    public function index(Request $request) {
        $locale = App::getLocale();
        if ($request->has('titlesearch')) {

            $items = Page::search($request->titlesearch)
                    ->paginate(6);
        } else {
            $items = Page::paginate(6);
        }


        return view('search.page-search', compact('items', 'locale'));
    }

    /**

     * Get the index name for the model.

     *

     * @return string

     */
    public function create(Request $request) {

        $this->validate($request, ['title' => 'required']);


        $items = Item::create($request->all());

        return back();
    }

}
