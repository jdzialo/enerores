<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Http\Requests\CreateContactFormRequest;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Slider\Entities\Slider;
use Modules\Media\Image\Imagy;


class PublicController extends BasePublicController {

    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var Application
     */
    private $app;
    
    private $images;
    
    private $pages;
    
      /**
     * @var SliderRepository
     */
    private $slider;
    
     /**
     * @var Imagy
     */
    private $imagy;

    public function __construct(PageRepository $page, Application $app, SliderRepository $slider , Imagy $imagy) {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
        $this->slider = $slider;
        $this->imagy = $imagy;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug) {
       
        $this->pages = $this->findPageForSlug($slug);   
       
        $this->throw404IfNotFound($this->pages);
        $this->images = $this->getImagesByPage( $this->pages);       
        $data = $this->loadData();      
        $template = $this->getTemplateForPage( $this->pages);  
        return view($template)->with($data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage() {
        
        $page = $this->page->findHomepage();  
        $this->pages = $page;
        $this->images = $this->getImagesByPage($page);
        $data = $this->loadData();
        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);
       
        return view($template)->with($data);
    }

    public function contactpage() {
        try {

            return view('contact.index');
        } catch (\Exception $e) {
           
            return redirect()->route("homepage")
                            ->withErrors("Wystąpił błąd" + $e->getMessage());
        }
    }

    public function storecontact(CreateContactFormRequest $request) {

        $reqestAll = $request->all();
       
        $this->email = $reqestAll['email'];

        \Mail::send('contact.email', $reqestAll, function($message) {
            $message->from($this->email);
            $message->to(env('MAIL_USERNAME'), 'Admin')->subject(trans('page::contact.mail.subject'));
        });

        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send contactform'));
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug) {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page) {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page) {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }

    private function getImagesByPage($page) {
         
        if (!is_null($page->files())) {
            $images = $page->files()->get();
          
            return $images;
        }
        return null;
    }
    

    public function mainSlider() {
        $slider = null;
        //$slides = $slider = Slider::with(['slides', 'slides.translations', 'slides.file'])->orderBy('created_at', 'desc')->first()->get()[0]->slides()->get();
        //$files = Slider::with(['slides', 'slides.translations', 'slides.file'])->first()->files()->get();
        $files = Slider::with(['slides', 'slides.translations', 'slides.file'])->first()->get()[0]->slides()->get();
        return $files;
    }

    public function loadData() {
        $data = [];
        $data['page'] = $this->pages;
        $default = $this->getCasualSlider();
        if(!is_null($default))
        {
            $data['defaultslider'] = $default;
        }
        if ($this->images != null) {
            $data['images'] = $this->images;
        }
        $slider = $this->mainSlider();        
        if (!is_null($slider)) {
            $data['slider'] = $slider;
            $data['imagy'] = $this->imagy;
        }     
       
        return $data;
    }
    
    public function getCasualSlider()
    {
      $slug = 'defaultslider';
      return  $this->slider->findBySlugInLocale($slug, $this->app->getLocale())->files()->get();
    }
    
    
   
    
}
